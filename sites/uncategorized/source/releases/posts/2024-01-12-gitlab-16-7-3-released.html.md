---
title: "GitLab Patch Release: 16.7.3 16.6.5 16.5.7"
categories: releases
author: Ahmad Tolba
author_gitlab: ahyield
author_twitter: gitlab
description: "GitLab releases 16.7.3 16.6.5 16.5.7 "
tags: patch releases, releases
---

<!-- For detailed instructions on how to complete this, please see https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/patch/blog-post.md -->

Today we are releasing versions `16.7.3` `16.6.5` `16.5.7` for GitLab Community Edition and Enterprise Edition.

These versions resolve a single issue with a database migration.

## GitLab Community Edition and Enterprise Edition

### 16.7.3

* [Make chat_names table migration idempotent](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/141704)

### 16.6.5

* [Make chat_names table migration idempotent](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/141705)

### 16.5.7

* [Make chat_names table migration idempotent](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/141706)


## Important notes on upgrading

This version fixes an [issue with an existing migration that prevented upgrades from completing](https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues/8371). It does not include any new migrations, and for multi-node deployments, [should not require any downtime](https://docs.gitlab.com/ee/update/#upgrading-without-downtime).

Please be aware that by default the Omnibus packages will stop, run migrations,
and start again, no matter how “big” or “small” the upgrade is. This behavior
can be changed by adding a [`/etc/gitlab/skip-auto-reconfigure`](https://docs.gitlab.com/ee/update/zero_downtime.html) file,
which is only used for [updates](https://docs.gitlab.com/omnibus/update/README.html).

## Updating

To update, check out our [update page](/update/).

## GitLab subscriptions

Access to GitLab Premium and Ultimate features is granted by a paid [subscription](/pricing/).

Alternatively, [sign up for GitLab.com](https://gitlab.com/users/sign_in)
to use GitLab's own infrastructure.
